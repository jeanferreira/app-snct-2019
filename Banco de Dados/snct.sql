-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 02-Out-2019 às 16:27
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `snct`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividades`
--

DROP TABLE IF EXISTS `atividades`;
CREATE TABLE IF NOT EXISTS `atividades` (
  `id_atividade` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tipo_atividade_id_tipo` int(10) UNSIGNED NOT NULL,
  `tipo_status_id_status` int(10) UNSIGNED NOT NULL,
  `usuarios_id_usuario` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(70) NOT NULL,
  `localizacao` varchar(50) NOT NULL,
  `responsavel` varchar(50) NOT NULL,
  `data_evento` date NOT NULL,
  `hora` time NOT NULL,
  `duracao` int(11) NOT NULL,
  PRIMARY KEY (`id_atividade`),
  KEY `atividades_FKIndex1` (`usuarios_id_usuario`),
  KEY `atividades_FKIndex2` (`tipo_status_id_status`),
  KEY `atividades_FKIndex3` (`tipo_atividade_id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `atividades`
--

INSERT INTO `atividades` (`id_atividade`, `tipo_atividade_id_tipo`, `tipo_status_id_status`, `usuarios_id_usuario`, `titulo`, `localizacao`, `responsavel`, `data_evento`, `hora`, `duracao`) VALUES
(1, 2, 2, 1, 'Minicurso de Alemão', 'Laboratório 311 - Bloco D', 'Patricia Cezar', '2019-10-21', '14:00:00', 2),
(2, 1, 1, 1, 'Minicurso de Arduino', 'Sala 305, Bloco D', 'Fulano de Tal', '2019-10-22', '18:00:00', 3),
(3, 3, 1, 3, 'Minicurso de Inglês', 'Sala 207, Bloco A', 'Ciclano de Freiras', '2019-10-23', '08:00:00', 4),
(4, 4, 1, 2, 'Reunião Prática de Carptintagem', 'Hall da UFAM', 'Carlos de Tal', '2019-10-24', '10:00:00', 2),
(5, 3, 1, 3, 'Exposição Artística da Drag Jean Ferracy', 'Hall da UFAM', 'Jean Ferreira ds Silva', '2019-10-25', '16:00:00', 2),
(6, 1, 1, 2, 'Palestra sobre sexualidade precoce', 'Sala, 311, Bloco A', 'Alexsander Renê', '2019-10-26', '08:00:00', 2),
(7, 4, 2, 3, 'Portas Abertas no Laboratório 312', 'Sala, 312, Bloco D', 'Odette Mestrinho', '2019-10-21', '08:00:00', 4),
(8, 4, 1, 1, 'Ei mana: mulheres na tecnologia', 'Sala, 311, Bloco D', 'Daniella Costa', '2019-10-21', '16:00:00', 2),
(9, 1, 3, 2, 'Minicurso de Violino', 'Sala 207, Bloco A', 'Alexsander Renê', '2019-10-21', '13:00:00', 3),
(10, 3, 2, 1, 'Palestra sobre arquitetura de Software', 'Sala 204, Bloco A', 'Ana Cardoso', '2019-10-21', '08:00:00', 1),
(11, 1, 1, 1, 'Naelly e Thais: Um tutorial de como vencer as eleições discente', 'Hall da UFAM', 'Naelly Pereira, Thais Carolyne', '2019-10-21', '10:00:00', 2),
(12, 8, 1, 1, 'Millena em: Como ser blogueira e sair plena de um incêndio', 'Hall da UFAM', 'Millena Sangela', '2019-10-21', '14:00:00', 4),
(13, 3, 3, 2, 'Meu Amor Lindo te Amo', 'meu coração', 'Você', '2019-10-21', '09:00:00', 5),
(14, 8, 1, 1, 'Como conquistar alguém que você quer', 'Sala 305, Bloco D', 'Euzineia', '2019-09-24', '08:00:00', 2),
(15, 7, 1, 2, 'Como dançar Boi', 'Sala, 311, Bloco A', 'Jean Ferreira da Silva', '2019-09-25', '14:00:00', 4),
(16, 6, 1, 2, 'Littera', 'Sala 207, Bloco A', 'Patricia Cezar', '2019-09-26', '18:00:00', 2),
(17, 5, 4, 3, 'Tutorial de como cozinhar e tempera soja', 'Sala, 311, Bloco D', 'Jean Ferreira da Silva', '2019-09-30', '09:00:00', 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

DROP TABLE IF EXISTS `noticias`;
CREATE TABLE IF NOT EXISTS `noticias` (
  `id_noticia` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `usuarios_id_usuario_2` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `data_hora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `imagem` varchar(255) NOT NULL,
  PRIMARY KEY (`id_noticia`),
  KEY `noticias_FKIndex1` (`usuarios_id_usuario_2`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `noticias`
--

INSERT INTO `noticias` (`id_noticia`, `usuarios_id_usuario_2`, `titulo`, `descricao`, `data_hora`, `imagem`) VALUES
(1, 1, 'Minicurso de Arduino', 'Este minicurso', '2019-08-21 19:54:00', 'androidstudio.png'),
(2, 2, 'Esta é uma notícia', 'A noticia', '2019-08-22 03:42:47', 'duplaface.png'),
(3, 1, 'Minicurso de Alemão', 'ksjdlkf sdf dfskf sdfdsjfsff sf s dkfs dskjlfsflslkdjsdlk jsldff fsfs dsfklsfss k sfsl fslfsklfslkfskls dfs f sdkfs klfskf ss fk sfdklf lks fs fs lkfsl kflks', '2019-08-27 01:28:06', 'pingo.jpg'),
(4, 3, 'O minicurso de Alemão foi realocado para sala 305, bloco D', 'A administração da snct acabou de informar que o minicurso foi alocado para a nova sala, que será 305, bloco D, porém o horário permanece o mesmo', '2019-08-29 19:53:28', 'bbuxraj.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_atividade`
--

DROP TABLE IF EXISTS `tipo_atividade`;
CREATE TABLE IF NOT EXISTS `tipo_atividade` (
  `id_tipo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `atividade` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipo_atividade`
--

INSERT INTO `tipo_atividade` (`id_tipo`, `atividade`) VALUES
(1, 'Palestra'),
(2, 'Minicurso'),
(3, 'Mesa Redonda'),
(4, 'Oficina'),
(5, 'Conferência'),
(6, 'Atividade de Extensão'),
(7, 'Atividade Cultural'),
(8, 'Amostra'),
(9, 'Outras Atividades');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_status`
--

DROP TABLE IF EXISTS `tipo_status`;
CREATE TABLE IF NOT EXISTS `tipo_status` (
  `id_tipo_status` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `andamento` varchar(20) NOT NULL,
  PRIMARY KEY (`id_tipo_status`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipo_status`
--

INSERT INTO `tipo_status` (`id_tipo_status`, `andamento`) VALUES
(1, 'Previsto'),
(2, 'Em andamento'),
(3, 'Realizado'),
(4, 'Cancelado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_usuario`
--

DROP TABLE IF EXISTS `tipo_usuario`;
CREATE TABLE IF NOT EXISTS `tipo_usuario` (
  `id_tipo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id_tipo`, `nome`) VALUES
(1, 'Administrador'),
(2, 'Colaborador');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tipo_usuario_id_tipo` int(10) UNSIGNED NOT NULL,
  `nome` varchar(20) NOT NULL,
  `sobrenome` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `senha` varchar(32) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `usuarios_FKIndex1` (`tipo_usuario_id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `tipo_usuario_id_tipo`, `nome`, `sobrenome`, `email`, `senha`) VALUES
(1, 2, 'Micaelle', 'Queiroz', 'micaellequeiroz6@gmail.com', '12345'),
(2, 1, 'Patricia', 'Cezar', 'pat@pat.com', '324354'),
(3, 2, 'Jean', 'Ferraira', 'jean@jean.com', 'jean');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `atividades`
--
ALTER TABLE `atividades`
  ADD CONSTRAINT `tipo_atividade_id_tipo` FOREIGN KEY (`tipo_atividade_id_tipo`) REFERENCES `tipo_atividade` (`id_tipo`),
  ADD CONSTRAINT `tipo_status_id_status` FOREIGN KEY (`tipo_status_id_status`) REFERENCES `tipo_status` (`id_tipo_status`),
  ADD CONSTRAINT `usuarios_id_usuario` FOREIGN KEY (`usuarios_id_usuario`) REFERENCES `usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `noticias`
--
ALTER TABLE `noticias`
  ADD CONSTRAINT `usuarios_id_usuario_2` FOREIGN KEY (`usuarios_id_usuario_2`) REFERENCES `usuarios` (`id_usuario`);

--
-- Limitadores para a tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `tipo_usuario_id_tipo` FOREIGN KEY (`tipo_usuario_id_tipo`) REFERENCES `tipo_usuario` (`id_tipo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
