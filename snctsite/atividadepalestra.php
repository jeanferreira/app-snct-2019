<?php
header('Content-Type: application/json; charset=utf-8');

if($_SERVER['REQUEST_METHOD'] == 'GET'){ 
    include 'connect.php';
    
    $con = New Connection();
    $conn = $con->getConnection();
    $stmt = $conn->query("SELECT atividades.data_evento, atividades.hora,tipo_atividade.atividade,atividades.titulo, atividades.localizacao, atividades.responsavel, tipo_status.andamento FROM atividades INNER JOIN tipo_atividade ON atividades.tipo_atividade_id_tipo = tipo_atividade.id_tipo INNER JOIN tipo_status ON atividades.tipo_status_id_status = tipo_status.id_tipo_status WHERE tipo_atividade.atividade = 'Palestra' ORDER BY tipo_status.id_tipo_status, atividades.data_evento,atividades.hora ASC");
    $resultado = $stmt->execute();

    $response = array();
 
    while ($atividade = $stmt->fetch($resultado)){ 
        $data = date("d/m", strtotime($atividade->data_evento));
        $hora = date("H:i", strtotime($atividade->hora));

        array_push($response, 
        array("dataEvento"=>$data,"tipoAtividade"=>$atividade->atividade, "titulo"=>$atividade->titulo, 
    "hora"=>$hora, "andamento"=>$atividade->andamento, "local"=>$atividade->localizacao, "responsavel"=>$atividade->responsavel));         
    }    
    }
echo json_encode(array("result"=>$response));
?>