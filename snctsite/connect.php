<?php
/**
 * User: Micaelle
 * Date: 20/08/2019
 * Time: 16:51
 */

class Connection
{
    private $server;
    private $user;
    private $pass;

    /**
     * Connection constructor.
     */
    public function __construct()
    {
        $ambiente = true;
        if($ambiente){//Ambiente de produção
            $this->server = 'mysql:host=108.179.193.168; dbname=conhe913_snct; charset=utf8';
            $this->user = 'conhe913_snct';
            $this->pass = 'snct1234';
        
        }else{
            $this->server = 'mysql:host=localhost; dbname=snct; charset=utf8';
            $this->user = '';
            $this->pass = '';    
        }
    }

 
    function getConnection(){
        try {
            $connect = new PDO($this->server, $this->user, $this->pass);
            return $connect;
        } catch (PDOException $ex) {
            echo 'Error:' . $ex->getMessage();
        }

    }
}