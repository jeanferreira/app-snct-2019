<?php
header('Content-Type: application/json; charset=utf-8');

if($_SERVER['REQUEST_METHOD'] == 'GET'){ 
    include 'connect.php';
    
    $con = New Connection();
    $conn = $con->getConnection();
    $stmt = $conn->query("SELECT tipo_atividade.atividade, atividades.titulo, atividades.hora, tipo_status.andamento, atividades.localizacao, atividades.responsavel FROM atividades INNER JOIN tipo_atividade ON atividades.tipo_atividade_id_tipo = tipo_atividade.id_tipo INNER JOIN tipo_status ON atividades.tipo_status_id_status = tipo_status.id_tipo_status WHERE data_evento = '2019-10-22' ORDER BY atividades.hora ASC");
    $resultado = $stmt->execute();

    $response = array();
  
    while ($programacao = $stmt->fetch($resultado)){ 
        $data = date("d/m", strtotime($programacao->data_evento));
        $hora = date("H:i", strtotime($programacao->hora));

        array_push($response, 
        array("tipoAtividade"=>$programacao->atividade, "titulo"=>$programacao->titulo, 
    "hora"=>$hora, "andamento"=>$programacao->andamento, "local"=>$programacao->localizacao, "responsavel"=>$programacao->responsavel));         
    }    
    }
echo json_encode(array("result"=>$response));
?>