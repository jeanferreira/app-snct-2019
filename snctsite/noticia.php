<?php
header('Content-Type: application/json; charset=utf-8');

if($_SERVER['REQUEST_METHOD'] == 'GET'){ 
    include 'connect.php';
    
    $con = New Connection();
    $conn = $con->getConnection();
    $stmt = $conn->query("SELECT noticias.id_noticia, noticias.titulo, noticias.data_hora, usuarios.nome, usuarios.sobrenome, noticias.descricao FROM noticias INNER JOIN usuarios ON noticias.usuarios_id_usuario_2 = usuarios.id_usuario ORDER BY noticias.id_noticia DESC");
    $resultado = $stmt->execute();

    $response = array();
  
    while ($noticia = $stmt->fetch($resultado)){ 
        $hora = date("d/m/Y  H:i", strtotime($noticia->data_hora));
        //10.82.3.19 icet
        //10.0.0.112 house
        //$imagem = "http://10.125.130.213/snctsite/imagens/".$noticia->imagem;
        array_push($response, 
        array("id"=>$noticia->id_noticia, "titulo"=>$noticia->titulo, 
    "data_hora"=>$hora, "nome"=>$noticia->nome, "sobrenome"=>$noticia->sobrenome,"descricao"=>$noticia->descricao));         
    }    
    }
echo json_encode(array("result"=>$response));
?> 
