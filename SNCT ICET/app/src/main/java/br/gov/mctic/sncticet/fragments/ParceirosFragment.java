package br.gov.mctic.sncticet.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.gov.mctic.snct.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ParceirosFragment extends Fragment {


    public ParceirosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_parceiros, container, false);
    }

}
