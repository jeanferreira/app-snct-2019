package br.gov.mctic.sncticet.fragments.atividade;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.gov.mctic.snct.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AtividadeFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_atividade, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ViewPager viewPager = view.findViewById(R.id.pager2);
        PagerAtividadeFragment pagerAtividadeFragment = new PagerAtividadeFragment(getChildFragmentManager());

        pagerAtividadeFragment.addFrag(new AtividadeGeralFragment(),"Geral");
        pagerAtividadeFragment.addFrag(new AtividadePalestraFragment(), "Palestras");
        pagerAtividadeFragment.addFrag(new AtividadeMinicursoFragment(),"Minicursos");
        pagerAtividadeFragment.addFrag(new AtividadeOficinaFragment(), "Oficinas");
        pagerAtividadeFragment.addFrag(new AtividadeConferenciaFragment(), "Conferências");
        pagerAtividadeFragment.addFrag(new AtividadeMesaRedFragment(),"Mesa Redonda");
        pagerAtividadeFragment.addFrag(new AtividadeAmostraFragment(), "Amostras");
        pagerAtividadeFragment.addFrag(new AtividadeExtensFragment(), "Atividade de Extensão");
        pagerAtividadeFragment.addFrag(new AtividadeCulturalFragment(), "Atividade Cultural");

        // When requested, this adapter returns a DemoObjectFragment,
        // representing an object in the collection.
        viewPager.setAdapter(pagerAtividadeFragment);
        
    }

    class PagerAtividadeFragment extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        PagerAtividadeFragment(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return mFragmentList.get(i);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
