package br.gov.mctic.sncticet.fragments.programacao;

import android.annotation.SuppressLint;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

// Since this is an object collection, use a FragmentStatePagerAdapter,
// and NOT a FragmentPagerAdapter.
public class PagerProgFragment extends FragmentStatePagerAdapter {
        private List<Fragment> fragmentList;

        PagerProgFragment(FragmentManager fm, List<Fragment> fragmentList) {
            super(fm);
            this.fragmentList = fragmentList;
        }

        @Override
        public Fragment getItem(int i) {
         return fragmentList.get(i);
        }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @SuppressLint("DefaultLocale")
    @Override
    public CharSequence getPageTitle(int position) {
        return String.format("2%d/10 ", position + 1);
    }

    }



