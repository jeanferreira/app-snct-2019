package br.gov.mctic.sncticet.primaryclasses;

public class Programacao {
    //Declaração das variáveis
    private String tipoMinicurso;
    private String hora;
    private String status;
    private String titulo;
    private String localizacao;
    private String responsavel;
    private String dataEvento;

    //Classe primitiva com as variáveis da Programacao. Buscando dados get() e inserindo nas variaveis set().
    public String getTipoMinicurso() {
        return tipoMinicurso;
    }
    public void setTipoMinicurso(String tipoMinicurso) {
        this.tipoMinicurso = tipoMinicurso;
    }
    public String getHora(){
        return hora;
    }
    public void setHora(String hora) {
        this.hora = hora;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public String getLocalizacao() {
        return localizacao;
    }
    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }
    public String getResponsavel() {
        return responsavel;
    }
    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }
    public String getDataEvento() {
        return dataEvento;
    }
    public void setDataEvento(String dataEvento) {
        this.dataEvento = dataEvento;
    }
}
