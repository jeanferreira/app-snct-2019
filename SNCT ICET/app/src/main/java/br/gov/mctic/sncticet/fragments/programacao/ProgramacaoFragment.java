package br.gov.mctic.sncticet.fragments.programacao;


import android.os.Bundle;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.gov.mctic.snct.R;


public class ProgramacaoFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_programacao, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ViewPager viewPager = view.findViewById(R.id.pager);
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new ProgData21Fragment());
        fragments.add(new ProgData22Fragment());
        fragments.add(new ProgData23Fragment());
        fragments.add(new ProgData24Fragment());
        fragments.add(new ProgData25Fragment());
        fragments.add(new ProgData26Fragment());

        // When requested, this adapter returns a DemoObjectFragment,
        // representing an object in the collection.
        PagerProgFragment pagerProgFragment = new PagerProgFragment(getChildFragmentManager(), fragments);
        viewPager.setAdapter(pagerProgFragment);
    }




}








