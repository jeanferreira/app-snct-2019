package br.gov.mctic.sncticet.fragments.atividade;

import android.annotation.SuppressLint;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

public class PagerAtividadeFragment extends FragmentStatePagerAdapter {
    private List<Fragment> fragmentList;
    private List<String> mFragmentTitleList;

    PagerAtividadeFragment(FragmentManager fm, List<Fragment> fragmentList, List<String> mFragmentTitleList) {
        super(fm);
        this.fragmentList = fragmentList;
        this.mFragmentTitleList = mFragmentTitleList;
    }

    @Override
    public Fragment getItem(int i) {
        return fragmentList.get(i);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    private void addFrag(String title) {
        mFragmentTitleList.add(title);
    }
    @SuppressLint("DefaultLocale")
    @Override
   public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }
}
