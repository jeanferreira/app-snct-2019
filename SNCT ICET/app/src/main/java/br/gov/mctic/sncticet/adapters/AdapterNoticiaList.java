package br.gov.mctic.sncticet.adapters;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.gov.mctic.snct.R;
import br.gov.mctic.sncticet.primaryclasses.Noticia;


public class AdapterNoticiaList extends RecyclerView.Adapter<AdapterNoticiaList.ViewHolder>{
    private List<Noticia> noticia;
    //Construtor da classe
    public AdapterNoticiaList(List<Noticia> noticia, FragmentActivity activity){
        super();
        this.noticia = noticia;
    }
    //Declaração do Layout do fragmento junto com view Holder, o suporte
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_noticia_list, parent, false);
        return new ViewHolder(view);
    }
    //insere as variáveis nos textos do XML capturando os dados da lista das Noticias
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Noticia n = noticia.get(position);
        //Deaclaração dos parâmetros que são buscados no banco de dados e inseridos em uma lista
        holder.textTitulo.setText(n.getTitulo());
        holder.textDataHora.setText("Publicado em "+n.getDataHora());
        holder.textNome.setText("• por "+n.getNome());
        holder.textSobrenome.setText(n.getSobrenome());
        holder.textDesc.setText(n.getDesc());

    }
    //Faz a contagem de acordo com o tamanho da lista
    @Override
    public int getItemCount() {
        return noticia.size();
    }
    //Declaração das variáveis e busca no XML pelo ID para o ViewHolder
    class ViewHolder extends RecyclerView.ViewHolder {
        CardView card;
        TextView textTitulo, textDataHora, textNome, textSobrenome, textDesc;

        ViewHolder(View itemView){
          super(itemView);
            //Busca pelo ID do card view onde será exibida as notícias
            card = itemView.findViewById(R.id.cardNoticia);
            //Textos do XML para exibição através do ID
            textTitulo = itemView.findViewById(R.id.txtTitulo);
            textDataHora = itemView.findViewById(R.id.txtDataHora);
            textNome = itemView.findViewById(R.id.txtNome);
            textSobrenome = itemView.findViewById(R.id.txtSobrenome);
            textDesc = itemView.findViewById(R.id.txtDescricao);
    }
}
}
