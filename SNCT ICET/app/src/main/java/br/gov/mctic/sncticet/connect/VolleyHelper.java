package br.gov.mctic.sncticet.connect;
/* Created by Micaelle Queiroz on 22/11/2016.*/

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

@SuppressLint("Registered")
public class VolleyHelper extends Application {
    //Declaração das variáveis de conexão
    @SuppressLint("StaticFieldLeak")
    private static VolleyHelper mInstance;
    private RequestQueue queue;
    private ImageLoader imageLoader;
    @SuppressLint("StaticFieldLeak")
    private static Context context;

    //Pega a requisição de PHP com JSON e Imagem
    private VolleyHelper(Context context) {
        VolleyHelper.context = context;
        queue = getRequestQueue();
        //Chama a classe para o carregamento de imagem

        imageLoader = new ImageLoader(this.queue, new ImageLoader.ImageCache() {
            private final LruBitmapCache mCache = new LruBitmapCache(10);
            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
    }
    //Retorna a instância do Contexto da requisição
    public static synchronized VolleyHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new VolleyHelper(context);
        }
        return mInstance;
    }
    //Captura a requisição
    public RequestQueue getRequestQueue() {
        if (queue== null) {
            Cache cache = new DiskBasedCache(context.getCacheDir(), 10 * 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            queue = new RequestQueue(cache, network);
            // Don't forget to start the volley request queue
            queue.start();
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            queue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return queue;
    }
    //Retorna a requisição adicionando os dados
    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
    //Retorna a requisição da imagem
    public ImageLoader getImageLoader() {
        return imageLoader;
    }

}