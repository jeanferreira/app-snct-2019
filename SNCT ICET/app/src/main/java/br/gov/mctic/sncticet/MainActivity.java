package br.gov.mctic.sncticet;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;

import br.gov.mctic.snct.R;
import br.gov.mctic.sncticet.fragments.atividade.AtividadeFragment;
import br.gov.mctic.sncticet.fragments.InfoFragment;
import br.gov.mctic.sncticet.fragments.NoticiaFragment;
import br.gov.mctic.sncticet.fragments.ParceirosFragment;
import br.gov.mctic.sncticet.fragments.programacao.ProgramacaoFragment;
import br.gov.mctic.sncticet.fragments.SobreFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    //Recarregar página
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Exbição da Activity de Notícias como página inicial do menu NavigationView
        getSupportFragmentManager().beginTransaction().replace(R.id.dLayout, new ProgramacaoFragment()).commit();
        //Declaração da barra de ferramentas pelo id do XML, encontra-se em app_bar_main.xml
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Declara o Layout da gaveta
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        //Ação que vai abrir e fechar os itens do menu
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }
    //Quando o layout do menu for pressionado
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        //Efeito de fechar e abrir o menu
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    //Declara o menu de itens
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    //Captura as opções do toolbar pelo seu ID
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_politica) {
            Intent i = new Intent(this, PoliticaActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //Captura os IDs do menu lateral e executam a ação de seleção quando pressionado
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        //Encaminha para o fragmento onde é exibida as notícias quando pressionado
        if (id == R.id.nav_noticias) {
            getSupportActionBar().setTitle(R.string.menu_noticias);
            getSupportFragmentManager().beginTransaction().replace(R.id.dLayout, new NoticiaFragment()).commit();
            //Encaminha para o fragmento onde é exibida as atividades quando pressionado
        } else if (id == R.id.nav_atividades) {
            getSupportActionBar().setTitle(R.string.menu_atividades);
            getSupportFragmentManager().beginTransaction().replace(R.id.dLayout, new AtividadeFragment()).commit();
            //Encaminha para o fragmento onde é exibida a programação quando pressionado
        } else if (id == R.id.nav_programacao) {
            getSupportActionBar().setTitle(R.string.menu_programaçao);
            getSupportFragmentManager().beginTransaction().replace(R.id.dLayout, new ProgramacaoFragment()).commit();
            //Encaminha para o fragmento onde é exibida a parceria quando pressionado
        } else if (id == R.id.nav_parceiros) {
            getSupportActionBar().setTitle(R.string.menu_parceiros);
            getSupportFragmentManager().beginTransaction().replace(R.id.dLayout, new ParceirosFragment()).commit();
            //Encaminha para o fragmento onde é exibida as informações quando pressionado
        } else if (id == R.id.nav_informacao) {
            getSupportActionBar().setTitle(R.string.menu_informacao);
            getSupportFragmentManager().beginTransaction().replace(R.id.dLayout, new InfoFragment()).commit();
            //Encaminha para o fragmento sobre o evento, quando pressionado
        }else if (id == R.id.nav_sobre) {
            getSupportActionBar().setTitle(R.string.menu_sobre);
            getSupportFragmentManager().beginTransaction().replace(R.id.dLayout, new SobreFragment()).commit();
            //Encaminha para a saída da aplicação
        }else if (id == R.id.nav_sair) {
            finish();
            //Compartilha o link para baixar o aplicativo por WhatsApp, SMS e Redes sociais
        } else if (id == R.id.nav_send) {

        }
        //Fecha o menu lateral quando não houver nenhuma dessas condições
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
