package br.gov.mctic.sncticet.adapters;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.gov.mctic.snct.R;
import br.gov.mctic.sncticet.primaryclasses.Programacao;

public class AdapterAtividadeList extends RecyclerView.Adapter<AdapterAtividadeList.ViewHolder> {
    private List<Programacao> programacao;

    //Construtor da classe
    public AdapterAtividadeList(List<Programacao> programacao){
        super();
        this.programacao = programacao;
    }

    //Declaração do Layout do fragmento junto com view Holder, o suporte
    @NonNull
    @Override
    public AdapterAtividadeList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_atividade_list, parent, false);
        return new AdapterAtividadeList.ViewHolder(view);
    }

    //insere as variáveis nos textos do XML capturando os dados da lista da Programacao
    @SuppressLint({"SetTextI18n", "ResourceAsColor"})
    public void onBindViewHolder(@NonNull AdapterAtividadeList.ViewHolder holder, int position) {
        //Caputra a estrutura get() e set() da classe primitiva
        Programacao p = programacao.get(position);
        //Deaclaração dos parâmetros que são buscados no banco de dados e inseridos em uma lista
        holder.textData.setText("Dia "+p.getDataEvento());
        holder.textHora.setText(p.getHora()+" horas");
        holder.textTipoMinicurso.setText("Categoria: "+p.getTipoMinicurso());
        holder.textTitulo.setText(p.getTitulo());
        holder.textLocal.setText("Local: "+ p.getLocalizacao());
        holder.textResp.setText("Responsável: "+p.getResponsavel());

        //holder.textStatus.setText(p.getStatus());
        String status;
        status = p.getStatus();
        switch (status) {
            case "Previsto":
                    holder.etiqueta.setBackgroundColor(Color.parseColor("#ff6400"));
                    holder.etiqueta.setText(p.getStatus());
                break;
            case "Em andamento":
                holder.etiqueta.setBackgroundColor(Color.parseColor("#009900"));
                holder.etiqueta.setText(p.getStatus());
                break;
            case "Realizado":
                holder.etiqueta.setBackgroundColor(Color.parseColor("#000099"));
                holder.etiqueta.setText(p.getStatus());
                break;
            default:
                holder.etiqueta.setBackgroundColor(Color.parseColor("#e61b26"));
                holder.etiqueta.setText(p.getStatus());
                break;
        }

    }

    @Override
    public int getItemCount() {
        return programacao.size();
    }

    //Declaração das variáveis e busca no XML pelo ID para o ViewHolder
    class ViewHolder extends RecyclerView.ViewHolder{
        CardView card;
        TextView textTipoMinicurso, textTitulo, textHora, textLocal, textResp, etiqueta, textData;
        ViewHolder (View view){
            super(view);
            //Busca pelo ID do card view onde será exibida a a programação
            card = view.findViewById(R.id.cardProgramacao);
            //Textos do XML para exibição através do ID
            textTipoMinicurso = view.findViewById(R.id.tTipoMinicurso);
            textTitulo = view.findViewById(R.id.tTitulo);
            textHora = view.findViewById(R.id.tHora);
            textLocal = view.findViewById(R.id.tLocal);
            textResp = view.findViewById(R.id.txtResp);
            etiqueta = view.findViewById(R.id.etiqueta);
            textData = view.findViewById(R.id.tData);
        }
    }
}


