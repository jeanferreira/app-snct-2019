package br.gov.mctic.sncticet.fragments.atividade;


import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.gov.mctic.snct.R;
import br.gov.mctic.sncticet.adapters.AdapterAtividadeList;
import br.gov.mctic.sncticet.connect.VolleyHelper;
import br.gov.mctic.sncticet.primaryclasses.Programacao;

/**
 * A simple {@link Fragment} subclass.
 */
public class AtividadeOficinaFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    //Cria-se uma Lista de programações
    private ArrayList<Programacao> listAtividade = new ArrayList<>();
    private RecyclerView rv;
    //Recarregar página
    private SwipeRefreshLayout refreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_atividade_oficina, container, false);
        //Recycler View para exbição dos dados
        rv = view.findViewById(R.id.rView);
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(layoutManager);
        //Recarregar página
        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(true);
                jsonRequest();
            }
        });

        //Permanecer na conexão
        onResume();
        return view;
    }

    private void jsonRequest() {
        //10.0.0.112 house
        //10.82.3.19 icet
        // Showing refresh animation before making http call
        refreshLayout.setRefreshing(true);
        //URL do projeto em PHP com JSON
        final String urlWeb = "https://www.conhecendomaues.com.br/sncticet/atividadeoficina.php";
        //Solicitar uma resposta de string da URL fornecida
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlWeb,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Busca as os dados do banco
                        getData(response);
                        // Stopping swipe refresh
                        refreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
            //Caso a conexão com o a Biblioteca Volley não funcione:
            @SuppressLint("NewApi")
            @Override
            public void onErrorResponse(VolleyError error) {
                // Stopping swipe refresh
                refreshLayout.setRefreshing(false);
                try {
                    jsonRequest();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        //Chama a Classe customizada do RequestQueue no contexto de um fragmento
        // para puxar por completo os dados
        VolleyHelper.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    private void getData(String response){
        listAtividade.clear();
        try {
            //Declara o Objeto do JSON
            JSONObject jsonObject = new JSONObject(response);
            //Armazena os dados do resultado do objeto em um ARRAY
            //vetor para armazenar o vetor em PHP via JSON
            JSONArray jArray = jsonObject.getJSONArray("result");

            for (int i = 0; i < jArray.length(); i++){
                //Captura os dados do Array
                JSONObject jsonObject1 = jArray.getJSONObject(i);
                //Chama a classe com a Lista dos dados primitivos
                Programacao programacao = new Programacao();
                //Insere os dados do banco de dados nas várias para exibição
                programacao.setTipoMinicurso(jsonObject1.getString("tipoAtividade"));
                programacao.setTitulo(jsonObject1.getString("titulo"));
                programacao.setLocalizacao(jsonObject1.getString("local"));
                programacao.setResponsavel(jsonObject1.getString("responsavel"));
                programacao.setDataEvento(jsonObject1.getString("dataEvento"));
                programacao.setHora(jsonObject1.getString("hora"));
                programacao.setStatus(jsonObject1.getString("andamento"));
                //Adiciona os dados na lista
                listAtividade.add(programacao);
                //Exibe a Lista no Recycler View
                RecyclerView.Adapter adapter = new AdapterAtividadeList(listAtividade);
                rv.setAdapter(adapter);
            }

            //Caso não funcione
        }catch (JSONException e){
            e.printStackTrace();
            Toast.makeText(getActivity(),"Falha na conexão, verifique seus dados ou Wi-Fi",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRefresh() {
        jsonRequest();
    }
}
