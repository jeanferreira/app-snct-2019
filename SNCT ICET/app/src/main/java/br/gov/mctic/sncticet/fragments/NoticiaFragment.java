package br.gov.mctic.sncticet.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.gov.mctic.snct.R;
import br.gov.mctic.sncticet.adapters.AdapterNoticiaList;
import br.gov.mctic.sncticet.connect.VolleyHelper;
import br.gov.mctic.sncticet.primaryclasses.Noticia;

public class NoticiaFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    //Cria-se uma Lista de notícias
    private ArrayList<Noticia> listNoticias = new ArrayList<>();
    //RecyclerViews para exibição dos dados
    private RecyclerView rv;
    //Recarregar página
    private SwipeRefreshLayout refreshLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.fragment_noticia, container, false);
        //Recycler View para exbição dos dados
        rv = view.findViewById(R.id.rView);
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(layoutManager);
        //Recarregar página
        refreshLayout = view.findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
               refreshLayout.setRefreshing(true);
               jsonRequest();
            }
        });
        //Permanecer na conexão
        onResume();
        return view;
    }

    private void jsonRequest() {
        //10.0.0.111 house
        //10.82.3.19 icet
        // Showing refresh animation before making http call
        refreshLayout.setRefreshing(true);
        //URL do projeto em PHP com JSON
        final String urlWeb = "https://www.conhecendomaues.com.br/sncticet/noticia.php";
        //Solicitar uma resposta de string da URL fornecida
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlWeb,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Busca as os dados do banco
                        getData(response);
                        // Stopping swipe refresh
                        refreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
            //Caso a conexão com o a Biblioteca Volley não funcione:
            @SuppressLint("NewApi")
            @Override
            public void onErrorResponse(VolleyError error) {
                // Stopping swipe refresh
                refreshLayout.setRefreshing(false);
                try {
                    jsonRequest();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        //Chama a Classe customizada do RequestQueue no contexto de um fragmento
        // para puxar por completo os dados
        VolleyHelper.getInstance(getContext()).addToRequestQueue(stringRequest);

    }

    private void getData(String response){
        listNoticias.clear();
        try {
            //Declara o Objeto do JSON
            JSONObject jsonObject = new JSONObject(response);
            //Armazena os dados do resultado do objeto em um ARRAY
            //vetor para armazenar o vetor em PHP via JSON
            JSONArray jArray = jsonObject.getJSONArray("result");
            //Executa-se uma estrutura de repetição de acordo com o tamanho do ARRAY
            for (int i = 0; i < jArray.length(); i++){
                //Captura os dados do Array
                JSONObject jsonObject1 = jArray.getJSONObject(i);
                //Chama a classe com a Lista dos dados primitivos
                Noticia noticia = new Noticia();
                //Insere os dados do banco de dados nas várias para exibição
                noticia.setId(jsonObject1.getString("id"));
                noticia.setTitulo(jsonObject1.getString("titulo"));
                noticia.setDataHora(jsonObject1.getString("data_hora"));
                noticia.setNome(jsonObject1.getString("nome"));
                noticia.setSobrenome(jsonObject1.getString("sobrenome"));
                noticia.setDesc(jsonObject1.getString("descricao"));

               /* SharedPreferences myPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor myEditor = myPreferences.edit();
                myEditor.putString("titulo",jsonObject1.getString("titulo"));
                myEditor.apply();*/

                //Adiciona os dados na lista
                listNoticias.add(noticia);
                //Exibe a Lista no Recycler View
                RecyclerView.Adapter adapter = new AdapterNoticiaList(listNoticias, getActivity());
                rv.setAdapter(adapter);
            }
        //Caso não funcione
        }catch (JSONException e){
            e.printStackTrace();
            //Toast.makeText(getActivity(),"Falha na conexão, verifique seus dados ou Wi-Fi",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRefresh() {
        jsonRequest();
    }
}
